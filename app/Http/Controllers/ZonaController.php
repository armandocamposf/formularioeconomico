<?php

namespace App\Http\Controllers;

use App\Models\Zona;
use Illuminate\Http\Request;

class ZonaController extends Controller
{
    public function getZonas(){
        return response()->json([
            'message' => "exito",
            'data' => Zona::all()
        ]);
    }

    public function storeZona(Request $request){
        $zona = Zona::create([
            'zona' => $request->zona
        ]);

        return response()->json([
            'status' => 200,
            'data' => $zona
        ]);
    }

    public function editZona( $idZona ){
        return response()->json([
            'status' => 200,
            'data' => Zona::find($idZona)
        ]);
    }

    public function updateZona( Request $request ){
        $zona = Zona::find($request->id);
        $zona->zona = $request->zona;
        $zona->save();

        return response()->json([
            'status' => 200,
            'data' => $zona
        ]);
    }
}
