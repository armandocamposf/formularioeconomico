<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Ayuda;

class AyudaController extends Controller
{
    public function getAyudas(){
        return response()->json([
            'message' => "exito",
            'data' => Ayuda::all()
        ]);
    }

    public function storeAyuda(Request $request)
    {
        $ayuda = Ayuda::create([
            'ayuda' => $request->ayuda
        ]);
    }

    public function editAyuda( $idAyuda ){
        $ayuda = Ayuda::find($idAyuda);

        return response()->json([
            'message' => "exito",
            'data' => $ayuda
        ]);
    }

    public function updateAyuda( Request $request ){
        $ayuda = Ayuda::find($request->idAyuda);
        $ayuda->ayuda = $request->ayuda;
        $ayuda->save();

        return response()->json([
            'message' => "exito",
            'data' => $ayuda
        ]);
    }
}
