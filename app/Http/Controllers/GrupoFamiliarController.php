<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\GrupoFamiliar;

class GrupoFamiliarController extends Controller
{
    public function get( $idInforme )
    {
        return response()->json([
            'data' => GrupoFamiliar::where('idInforme', $idInforme)->get()
        ]);
    }

    public function store( Request $request, $idInforme )
    {
        $data = new GrupoFamiliar();
        $data->idInforme = $idInforme;
        $data->nombres = $request->nombres_fa;
        $data->edad = $request->edad_fa;
        $data->parentesco = $request->parentesco;
        $data->colegio = $request->colegio;
        $data->curso = $request->curso;
        $data->ocupacion = $request->ocupacion;
        $data->ingresos = $request->ingresos_fa;
        $data->discapacidad = $request->discapacidad_fa;
        $data->save();

        return $data;
    }
}
