<?php

namespace App\Http\Controllers;


use App\Models\Barrio;
use Illuminate\Http\Request;

class BarrioController extends Controller
{
    public function getBarrios( $idZona ){
        return response()->json([
            'message' => "exito",
            'data' => Barrio::where('idZona', $idZona)->get()
        ]);
    }

    public function storeBarrio( Request $request ){
        $barrio = Barrio::create([
            'barrio' => $request->barrio,
            'idZona' => $request->idZona
        ]);

        return response()->json([
            'status' => 200,
            'data' => $barrio
        ]);
    }

    public function editBarrio( $idBarrio ){
        $barrio = Barrio::find($idBarrio);
        return response()->json([
            'status' => 200,
            'data' => $barrio
        ]);
    }

    public function updateBarrio( Request $request ){
        $barrio = Barrio::find($request->idBarrio);
        $barrio->barrio = $request->barrio;
        $barrio->save();

        return response()->json([
            'status' => 200,
            'data' => $barrio
        ]);
    }
}
