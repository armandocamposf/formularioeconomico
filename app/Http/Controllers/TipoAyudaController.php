<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TipoAyuda;

class TipoAyudaController extends Controller
{
    public function getTipos( $idAyuda )
    {
        return response()->json([
            'status' => 200,
            'data' => TipoAyuda::where('idAyuda', $idAyuda)->get()
        ]);
    }

    public function storeTipo(Request $request)
    {
        $tipo = TipoAyuda::create([
            'tipo_ayuda' => $request->tipo_ayuda,
            'idAyuda' => $request->idAyuda
        ]);

        return response()->json([
            'status' => 200,
            'data' => $tipo
        ]);
    }

    public function editTipo( $idTipoAyuda )
    {
        $tipo = TipoAyuda::find($idTipoAyuda);

        return response()->json([
            'status' => 200,
            'data' => $tipo
        ]);
    }

    public function updateTipo( Request $request )
    {
        $tipo = TipoAyuda::find($request->idTipoAyuda);
        $tipo->tipo_ayuda = $request->tipo_ayuda;
        $tipo->save();

        return response()->json([
            'status' => 200,
            'data' => $tipo
        ]);
    }
}
