<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hash;
use App\Models\User;
use Auth;

class UserController extends Controller
{
    public function login(Request $request){
        if(!Auth::attempt(['email' => $request->email, 'password' => $request->password]))
        {
            return redirect('/');
        }
        else{
            return redirect('/');
        }

    }

    public function logout(){
        Auth::logout();
        return redirect('/');
    }

    public function getUsers(){
        return response()->json([
            'message' => "exito",
            'data' => User::all()
        ]);
    }

    public function storeUser(Request $request){
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'tipo_usuario' => $request->tipo,
            'status' => $request->status
        ]);

        return response()->json([
            'status' => 200,
            'data' => $user
        ]);
    }

    public function inactivateUser( $idUser ){
        $user = User::find($idUser);
        $user->status = 2;
        $user->save();
    }

    public function activateUser( $idUser ){
        $user = User::find($idUser);
        $user->status = 1;
        $user->save();
    }

    public function editUser( $idUser ){
        $user = User::find($idUser);

        return response()->json([
            'status' => 200,
            'data' => $user
        ]);
    }

    public function updateUser(Request $request){
        $user = User::find($request->idUser);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->tipo_usuario = $request->tipo;
        if($request->password != ""){
            $user->password = Hash::make($request->password);
        }
        $user->save();

        return response()->json([
            'status' => 200,
            'data' => $user
        ]);
    }
}
