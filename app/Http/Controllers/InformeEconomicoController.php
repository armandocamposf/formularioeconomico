<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\InformeEconomico;

class InformeEconomicoController extends Controller
{
    public function dataBasica( Request $request )
    {
        $data = new InformeEconomico();
        $data->fecha = date('Y-m-d');
        $data->nombres = $request->nombres;
        $data->apellidos = $request->apellidos;
        $data->dni = $request->dni;
        $data->edad = $request->edad;
        $data->idzona = $request->zona;
        $data->idBarrio = $request->barrio;
        $data->calle = $request->calle;
        $data->telefono_fijo = $request->telefono;
        $data->telefono_celular = $request->celular;
        $data->estudios = $request->estudio;
        $data->ocupacion = $request->ocupacion;
        $data->ingresos = $request->ingresos;
        $data->discapacidad = $request->discapacidad;
        $data->enfermedad_cronica = $request->enfermedad;
        $data->enfermedad_cronica_detalle = $request->detalle_enfermedad;
        $data->obra_social = $request->obra;
        $data->obra_social_detalle = $request->detalle_obra;
        $data->save();

        return response()->json([
            'idInforme' => $data->id
        ]);
    }

    public function storeData( Request $request, $idInforme )
    {
        $data = InformeEconomico::find($idInforme);
        $data->tipo_vivienda = $request->tipo_vivienda;
        $data->tiempo_vivienda = $request->tiempo_vivienda;
        $data->monto_alquiler = $request->monto_alquiler;
        $data->material_vivienda = $request->material_vivienda;
        $data->cantidad_dormitorios = $request->dormitorios;
        $data->cantidad_camas = $request->camas;
        $data->nucleo_humero =$request->nucleo;
        $data->dentro_vivienda = $request->dentro;
        $data->material_nucleo = $request->material;
        $data->luz = $request->luz;
        $data->agua = $request->agua_corriente;
        $data->gas = $request->gas;
        $data->cloacas = $request->cloacas;
        $data->internet = $request->internet;
        $data->ingreso_familiar = $request->ingreso_val;
        $data->provenencia = $request->provenencia;
        $data->idAyuda = $request->ayuda;
        $data->idTipoAyuda = $request->tipo_ayuda;
        $data->estado = 1;
        $data->save();

        return $data;
    }

    public function get()
    {
        return response()->json([
            'data' => InformeEconomico::all()
        ]);
    }

    public function informesByEstado($estado)
    {
        return response()->json([
            'data' => InformeEconomico::whereEstado($estado)->get()
        ]);
    }

    public function informesByFecha($fecha)
    {
        $fechas = explode(',', $fecha);

        return response()->json([
            'data' => InformeEconomico::whereBetween('fecha', [$fechas[0], $fechas[1]])->get()
        ]);
    }

    public function getDetalle( $idInforme )
    {
        $data = InformeEconomico::where('id', $idInforme)->with(['ayuda', 'tipoAyuda', 'zona', 'barrio'])->first();

        return response()->json([
            'data' => $data
        ]);
    }

    public function updateEstado( Request $request )
    {
        $inf = InformeEconomico::find($request->idInforme);
        $inf->estado = $request->estado;
        $inf->save();

        return 1;
    }

    public function updateObservaciones( Request $request )
    {
        $inf = InformeEconomico::find($request->idInforme);
        $inf->observaciones = $request->observaciones;
        $inf->save();

        return 1;
    }
}
