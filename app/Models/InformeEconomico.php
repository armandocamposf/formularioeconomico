<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InformeEconomico extends Model
{
    use HasFactory;

    public function ayuda()
    {
        return $this->hasOne(Ayuda::class, 'id', 'idAyuda');
    }

    public function tipoAyuda()
    {
        return $this->hasOne(TipoAyuda::class, 'id', 'idTipoAyuda');
    }

    public function zona()
    {
        return $this->hasOne(Zona::class, 'id', 'idZona');
    }

    public function barrio()
    {
        return $this->hasOne(Barrio::class, 'id', 'idBarrio');
    }
}
