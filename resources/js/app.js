require('./bootstrap');

window.Vue = require('vue').default;

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

// Import Bootstrap and BootstrapVue CSS files (order is important)
import 'bootstrap-vue/dist/bootstrap-vue.css'

import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI);

// Make BootstrapVue available throughout your project
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)

import VueRouter from 'vue-router'
Vue.use(VueRouter)

import VueFormWizard from 'vue-form-wizard'
import 'vue-form-wizard/dist/vue-form-wizard.min.css'
Vue.use(VueFormWizard)


import VueSweetalert2 from "vue-sweetalert2"
Vue.use(VueSweetalert2)
import "sweetalert2/dist/sweetalert2.min.css"

import App from './components/App.vue'
import Index from './components/Index.vue'
import IndexUsuario from './components/Usuarios/Index.vue'
import CreateUsuario from './components/Usuarios/Create.vue'
import EditUsuario from './components/Usuarios/Edit.vue'
import Zonas from './components/Zonas/Zonas.vue'
import CreateZona from './components/Zonas/CreateZona.vue'
import EditZona from './components/Zonas/EditZona.vue'
import Barrios from './components/Zonas/Barrios.vue'
import CreateBarrio from './components/Zonas/CreateBarrio.vue'
import EditBarrio from './components/Zonas/EditBarrio.vue'
import Ayudas from './components/Ayudas/Index.vue'
import CreateAyuda from './components/Ayudas/Create.vue'
import EditAyuda from './components/Ayudas/Edit.vue'
import TiposAyudas from './components/TiposAyudas/Index.vue'
import CreateTipoAyuda from './components/TiposAyudas/Create.vue'
import EditTipoAyuda from './components/TiposAyudas/Edit.vue'
import InformesEconomicos from './components/InformeEconomico/Index.vue'
import CrearInforme from './components/InformeEconomico/CrearInforme.vue'
import DetalleInforme from './components/InformeEconomico/DetalleInforme.vue'

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            component: Index,
            name: 'index'
        },
        {
            path: '/usuarios',
            component: IndexUsuario,
            name: 'usuario.index',
        },
        {
            path: '/user/create',
            component: CreateUsuario,
            name: 'usuario.create',
        },
        {
            path: '/user/edit/:id',
            component: EditUsuario,
            name: 'usuario.edit',
        },
        {
            path: '/zonas',
            component: Zonas,
            name: 'zonas.index',
        },
        {
            path: '/zona/create',
            component: CreateZona,
            name: 'zonas.create',
        },
        {
            path: '/zona/edit/:id',
            component: EditZona,
            name: 'zonas.edit',
        },
        {
            path: '/barrios/:id',
            component: Barrios,
            name: 'barrios.index',
        },
        {
            path: '/barrio/create/:id',
            component: CreateBarrio,
            name: 'barrio.create',
        },
        {
            path: '/barrio/edit/:id',
            component: EditBarrio,
            name: 'barrio.edit',
        },
        {
            path: '/ayudas',
            component: Ayudas,
            name: 'ayudas.index',
        },
        {
            path: '/ayuda/create',
            component: CreateAyuda,
            name: 'ayuda.create',
        },
        {
            path: '/ayuda/edit/:id',
            component: EditAyuda,
            name: 'ayuda.edit',
        },
        {
            path: '/tipos-ayudas/:id',
            component: TiposAyudas,
            name: 'tipo-ayuda.index',
        },
        {
            path: '/tipo-ayuda/create/:id',
            component: CreateTipoAyuda,
            name: 'tipo-ayuda.create',
        },
        {
            path: '/informes-economicos',
            component: InformesEconomicos,
            name: 'informes.index',
        },
        {
            path: '/crear-informe-economico',
            component: CrearInforme,
            name: 'informe.create',
        },
        {
            path: '/detalle-informe/:idInforme',
            component: DetalleInforme,
            name: 'informe.detalle'
        }

    ]
})

Vue.component('pagination', require('laravel-vue-pagination'));

const app = new Vue({
    el: '#app',
    components: { App },
    router
});
