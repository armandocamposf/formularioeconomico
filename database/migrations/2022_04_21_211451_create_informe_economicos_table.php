<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInformeEconomicosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('informe_economicos', function (Blueprint $table) {
            $table->id();
            $table->date('fecha')->nullable();
            $table->string('nombres')->nullable();
            $table->string('apellidos')->nullable();
            $table->string('dni')->nullable();
            $table->integer('edad')->nullable();
            $table->integer('idZona')->nullable();
            $table->integer('idBarrio')->nullable();
            $table->string('calle')->nullable();
            $table->string('telefono_fijo')->nullable();
            $table->string('telefono_celular')->nullable();
            $table->string('estudios')->nullable();
            $table->string('ocupacion')->nullable();
            $table->integer('ingresos')->nullable();
            $table->integer('discapacidad')->nullable();
            $table->integer('enfermedad_cronica')->nullable();
            $table->string('enfermedad_cronica_detalle')->nullable();
            $table->integer('obra_social')->nullable();
            $table->string('obra_social_detalle')->nullable();
            $table->string('tipo_vivienda')->nullable();
            $table->string('monto_alquiler')->nullable();
            $table->string('tiempo_vivienda')->nullable();
            $table->string('material_vivienda')->nullable();
            $table->string('cantidad_dormitorios')->nullable();
            $table->string('cantidad_camas')->nullable();
            $table->integer('nucleo_humero')->nullable();
            $table->integer('dentro_vivienda')->nullable();
            $table->string('material_nucleo')->nullable();
            $table->string('luz')->nullable();
            $table->string('agua')->nullable();
            $table->string('gas')->nullable();
            $table->string('cloacas')->nullable();
            $table->string('internet')->nullable();
            $table->integer('ingreso_familiar')->nullable();
            $table->string('provenencia')->nullable();
            $table->integer('idAyuda')->nullable();
            $table->integer('idTipoAyuda')->nullable();
            $table->string('observaciones')->nullable();
            $table->integer('estado')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('informe_economicos');
    }
}
