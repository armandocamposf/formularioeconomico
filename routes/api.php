<?php

use App\Http\Controllers\BarrioController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ZonaController;
use App\Http\Controllers\AyudaController;
use App\Http\Controllers\GrupoFamiliarController;
use App\Http\Controllers\InformeEconomicoController;
use App\Http\Controllers\TipoAyudaController;
use App\Models\GrupoFamiliar;
use App\Models\InformeEconomico;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'v1'], function() {
    Route::group(['prefix' => 'usuario'], function() {
        Route::get('get', [UserController::class, 'getUsers']);
        Route::post('store', [UserController::class, 'storeUser']);
        Route::get('inactivate/{idUser}', [UserController::class, 'inactivateUser']);
        Route::get('activate/{idUser}', [UserController::class, 'activateUser']);
        Route::get('edit/{idUser}', [UserController::class, 'editUser']);
        Route::post('update', [UserController::class, 'updateUser']);
    });


    Route::group(['prefix' => 'zonas'], function() {
        Route::get('get', [ZonaController::class, 'getZonas']);
        Route::post('store', [ZonaController::class, 'storeZona']);
        Route::get('edit/{idZona}', [ZonaController::class, 'editZona']);
        Route::post('update', [ZonaController::class, 'updateZona']);
    });

    Route::group(['prefix' => 'barrios'], function() {
        Route::get('/{idZona}', [BarrioController::class, 'getBarrios']);
        Route::post('store', [BarrioController::class, 'storeBarrio']);
        Route::get('edit/{idBarrio}', [BarrioController::class, 'editBarrio']);
        Route::post('update', [BarrioController::class, 'updateBarrio']);
    });

    Route::group(['prefix' => 'ayudas'], function() {
        Route::get('get', [AyudaController::class, 'getAyudas']);
        Route::post('store', [AyudaController::class, 'storeAyuda']);
        Route::get('edit/{idAyuda}', [AyudaController::class, 'editAyuda']);
        Route::post('update', [AyudaController::class, 'updateAyuda']);
    });

    Route::group(['prefix' => 'tipos-ayudas'], function() {
        Route::get('get/{idAyuda}', [TipoAyudaController::class, 'getTipos']);
        Route::post('store', [TipoAyudaController::class, 'storeTipo']);
        Route::get('edit/{idTipoAyuda}', [TipoAyudaController::class, 'editTipo']);
        Route::post('update', [TipoAyudaController::class, 'updateTipo']);
    });

    Route::group(['prefix' => 'informes'], function () {
        Route::get('/', [InformeEconomicoController::class, 'get']);
        Route::post('/dataBasica', [InformeEconomicoController::class, 'dataBasica']);
        Route::post('/storeData/{idInforme}', [InformeEconomicoController::class, 'storeData']);
        Route::get('detalle/{idInforme}', [InformeEconomicoController::class, 'getDetalle']);
        Route::post('updateEstado', [InformeEconomicoController::class, 'updateEstado']);
        Route::post('updateObservaciones',[InformeEconomicoController::class, 'updateObservaciones']);
        Route::get('ByEstado/{estado}', [InformeEconomicoController::class, 'informesByEstado']);
        Route::get('ByFecha/{fecha}', [InformeEconomicoController::class, 'informesByFecha']);
    });

    Route::group(['prefix' => 'grupo-familiar'], function () {
        Route::get('get/{idInforme}', [GrupoFamiliarController::class, 'get']);
        Route::post('store/{idInforme}', [GrupoFamiliarController::class, 'store']);
    });
});
